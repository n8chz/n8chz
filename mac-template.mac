/* See http://riotorto.users.sourceforge.net/Maxima/gnuplot/region/index.html */

load(implicit_plot);

file_name : "4-1-2";

constraints : [
  2*x_1+x_2<=6,
  x_1+2*x_2<=6,
  x_1>=0,
  x_2>=0
];

lines(constraints) := map(lambda([x], lhs(x) = rhs(x)), constraints);


l : lines(constraints);

combinations(s, n) :=
  if (n <= 0) then ([])
  else (
    if (n = 1) then (map(lambda([x], makelist(x, 1)), s))
    else (
      if (length(s) <= n) then ([s])
      else (
        append(
          map(lambda([x], cons(first(s), x)), combinations(rest(s), n-1)),
          combinations(rest(s), n)
        )
      )
    )
  );

andify(c) := if (length(c) <= 1) then (c[1]) else (c[1] and andify(rest(c)));

solutions(constraints) :=
  (
    a : map(
      lambda([x], solve(x, [x_1, x_2])),
      combinations(lines(constraints), 2)
    ),
    map(first, delete([], a)) /* kludge */
  );

s: solutions(constraints);

corners:
  sublist(
    solutions(constraints),
    lambda([x], sublis(x, andify(constraints)))
  );

intersections(constraints) :=
  (
    map(lambda([x], map(rhs, x)), solutions(constraints))
  );

i : map(lambda([x], map(rhs, x)), corners);

x_min : lmin(map(first, i));
x_max : lmax(map(first, i));
y_min : lmin(map(second, i));
y_max : lmax(map(second, i));
x_mid : (x_min+x_max)/2;
y_mid : (y_min+y_max)/2;
x_range : cons('x_1, 1.1*([x_min, x_max]-x_mid)+x_mid);
y_range : cons('x_2, 1.1*([y_min, y_max]-y_mid)+y_mid);

terms: map(lambda([s], (
  x : first(s),
  y : second(s),
  printf(false, "set label '(~a, ~a)' at ~f,~f point pointtype 6", x, y, x, y)
)), i);

preamble: concat("set zeroaxis;", printf(false, "~{~a~^;~}~%", terms));


implicit_plot(
  lines(constraints),
  x_range,
  y_range,
  [svg_file, printf(false, "./~a.svg", file_name)],
  [gnuplot_preamble, preamble]);
