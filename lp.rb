require "matrix"

class LP
  attr_accessor :a, :b, :c, :m, :n, :tableau

  def initialize(a, b, c)
    parse_args(a, b, c)
    initial_tableau
  end

  def self.from_tableau(t)
    tableau = Matrix.rows(t)
    m = tableau.row_count-1
    cc = tableau.column_count-1
    n = cc-m-1 # kludge?
    a = tableau.block(1..m, 1..n)
    b = tableau.block(1..m, cc..cc)
    c = tableau.block(0..0, 1..n)
    lp = self.new(a, b, c)
    lp.tableau = tableau
    puts "initial tableau: #{tableau}"
    # lp.m = m
    # lp.n = n
    lp
  end

  def simplex(slim=false)
    if slim
      puts "thead"
      puts "  tr"
      puts "    td"
      puts "    td \\(Z\\)"
      (1..@m+@n).each do |j|
        puts "    td \\(x_#{j}\\)"
      end
      puts "    td RHS"
      puts "    td ratio"
    end
    simplex_loop(slim)
    extract_optimum(slim)
  end

  def slim_tbody(ev=nil, lvr=nil, ratio = false)
    s = "tbody.simplex\n"
    (0..@m).each do |i|
      s << "  tr"
      s << ".lv" if i.positive? && lvr == i
      s << "\n"
      s << "    td"
      s << " \\(x_#{@bv[i-1]}\\)" if i.positive?
      s << "\n"
      (0..@m+@n+1).each do |j|
        s << "    td"
        s << ".ev" if ev && ev == j
        s << " #{@tableau[i, j].tex}"
        s << "\n"
      end
      s << "    td"
      if ratio && !@tableau[i, ev].zero? && !i.zero?
        s << " #{(@tableau[i, @m+@n+1]/@tableau[i, ev]).tex}"
      end
      s << "\n"
    end
    s
  end

  def algebraic_interpretation
    puts "div.ai.a"
    initialization
    iteration = 1
    (1..).each do |iteration|
      puts "  div.iteration"
      puts "    | Optimality test"
      return true if optimal?
      puts "    br"
      puts "    | Iteration #{iteration}"
      puts "    div.step"
      puts "      | Step 1"
      ev = move_up_edge
      puts "      | Step 2"
      lv = stop_when(ev)
      puts "      | Step 3"
      find_intersection(ev, lv)
    end
  end

  private

  def initialization
    initial_tableau
    inbv = (0...@n-1).map do |j|
      "\\(x_#{@bv[j]}\\)"
    end.join(", ")+" and \\(x_#{@bv[@n-1]}\\)"
    ibfs = [0, 0]
    ibfs << (1..@m).map {|i| @tableau[i, @m+@n+1]}
    puts "  div.text"
    puts "    ' Choose"
    puts "    ' #{inbv}"
    puts "    ' to be the nonbasic variables (= 0)"
    puts "    ' for the initial BF solution:"
    puts "    ' (#{ibfs.join(', ')})."
  end

  def bfs
    (1..@m+@n).map do |j|
      if @bv.include?(j)
        @tableau[@bv.index(j)+1, @m+@n+1].tex
      else
        "0"
      end
    end.join(", ")
  end

  def optimal?
    zcoeff = @tableau.block(0..0, 1..@m+@n)
    opt = zcoeff.none?(&:negative?)
    eoa = @n == 2 ? "either" : "any"
    nbv = (1...@n).map {|j| "\\(x_#{@nbv[j-1]+1}\\)"}.join(", ")
    nbv = "("+nbv+", or \\(x_#{@nbv.last}\\))"
    if opt
      puts "    div.text"
      puts "      ' (#{bfs}) is optimal, because increasing"
      puts "      ' #{eoa} nonbasic variable"
      puts "      ' (#{nbv}) decreases \\(Z\\)."
    else
      puts "    div.text"
      puts "      ' Not optimal, because increasing #{eoa}"
      puts "      ' nonbasic variable #{nbv}"
      puts "      ' increases \\(Z\\)."
    end
    opt
  end

  def move_up_edge
    ev = (1..@m+@n).min_by {|j| @tableau[0, j]}
    puts "      div.text"
    puts "        ' Increase \\(x_#{ev}\\) while adjusting"
    puts "        ' other variable values to satisfy"
    puts "        ' the system of equations."
    ev
  end

  def stop_when(ev)
    bv = (1...@m).map {|j| "\\(x_#{@bv[j-1]}\\)"}.join(", ")
    bv = "("+bv+", or \\(x_#{@bv.last}\\))"
    lvr = (1..@m).min_by do |i|
      tiev = @tableau[i, ev]
      tiev.positive? ? @tableau[i, @m+@n+1]/tiev : Float::INFINITY
    end
    if lvr == Float::INFINITY
      puts "      div.text"
      puts "        ' \\(x_#{ev}\\) can be increased indefinitely."
      puts "        ' \\(Z\\) is unbounded."
    end
    zero_out(lvr, ev, false) # Is this where this should go?
    lv = @bv[lvr-1]
    puts "      div.text"
    puts "        ' Stop when the first basic variable"
    puts "        ' #{bv}"
    puts "        ' drops to zero (\\(x_#{lv}\\))."
    lv
  end

  def find_intersection(ev, lv)
    @bv[@bv.index(lv)] = ev
    @nbv[@nbv.index(ev)] = lv
    puts "      div.text"
    puts "        ' With \\(x_#{ev}\\) now a basic variable"
    puts "        ' and \\(x_#{lv}\\) now a nonbasic variable,"
    puts "        ' solve the system of equations:"
    puts "        ' (#{bfs}) is the new BF solution."
    bfs
  end

  def simplex_loop(slim=false)
    while some_neg_c?
      ev = find_current_ev
      lvr = find_current_lvr(ev = find_current_ev, slim)
=begin
      unless lvr
        print slim ? @tableau.slim_tbody : @tableau.display
        raise "unbounded Z"
      end
=end
      if slim
        print slim_tbody(ev, lvr, ratio=true)
      else
        puts
        print @tableau.display
      end
      lv = @bv[lvr-1]
      @bv[@bv.index(lv)] = ev
      @nbv[@nbv.index(ev)] = lv
      zero_out(lvr, ev, slim)
    end
    self
  end

  def parse_args(a, b, c, d=nil, e=nil) # d >=, e = constraints
    @a = a.instance_of?(Matrix) ? a : Matrix.rows(a)
    @b = b.instance_of?(Matrix) ? b : Matrix.column_vector(b)
    @c = c.instance_of?(Matrix) ? c : Matrix.row_vector(c)
    @m = @a.row_count
    @n = @a.column_count
    if @b.row_count != @m || @c.column_count != @n
      msg = "#{@b.row_count} resource constraints, "
      msg << "#{@c.column_count} objective coefficients, "
      msg << "but #{@m}x#{@n} constraint matrix"
      raise msg
    end
    @nbv = (1..@n).to_a
    @bv = (@n+1..@n+@m).to_a
    self
  end

  def initial_tableau
    top_row = Matrix[[1]].hstack(-@c).hstack(Matrix.zero(1, @m+1))
    @tableau = top_row.vstack(
      Matrix.zero(@m, 1)
        .hstack(@a)
        .hstack(Matrix.I(@m))
        .hstack(@b))
    self
  end

  def some_neg_c?
    @tableau.block(0..0, 1..@m+@n).any?(&:negative?)
  end

  def extract_optimum(slim=false)
    if slim
      print slim_tbody
    else
      puts
      print @tableau.display
    end
    (1..@n).map do |j|
      @bv.include?(j) ? @tableau[@bv.index(j)+1, @m+@n+1] : 0
    end
  end

  def find_current_ev
    ev = (1..@m+@n).min_by {|j| @tableau[0, j]}
  end
  
  def find_current_lvr(ev, slim=false)
    if @tableau.block(1..@m, ev..ev).none?(&:positive?)
      print slim ? slim_tbody : @tableau.display
      raise "unbounded objective"
    end
    if (1..@m).none? {|i| @tableau[i, ev].positive?}
      return nil
    end
    lv_row = (1..@m).min_by do |i|
      foo = @tableau[i, ev]
      foo.positive? ? Rational(@tableau[i, @m+@n+1], foo) : Float::INFINITY
    end
    lv_row
  end

  def zero_out(lvr, ev, slim=false)
    divisor = @tableau[lvr, ev]
    if slim
      output =  "  tr\n"
      output << "    td colspan=\"#{@m+@n+4}\" now to divide row #{lvr}"
      output << " (representing \\(x_#{@bv[lvr-1]}\\)) by #{divisor.tex}:\n"
      puts output
    end
    (1..@m+@n+1).each do |j|
      @tableau[lvr, j] = Rational(@tableau[lvr, j], divisor)
    end
    if slim
      print slim_tbody
    else
      # print @tableau.display
    end

    (0..@m).each do |i|
      next if i == lvr
      multiplier = @tableau[i, ev]
      (1..@m+@n+1).each do |j|
        @tableau[i, j] -= multiplier*@tableau[lvr, j]
      end
    end
    self
  end
end

class Numeric
  def tex
    rat = Rational(self.abs)
    str = if rat.denominator == 1
      rat.numerator.to_s
    else
      "\\frac{#{rat.numerator}}{#{rat.denominator}}"
    end
    sgn = negative? ? "-" : ""
    "\\(#{sgn}#{str}\\)"
  end
end

class Matrix
  def block(row_range, col_range)
    rr = row_range.to_a
    cr = col_range.to_a
    Matrix.build(rr.count, cr.count) do |i, j|
      self[rr[i], cr[j]]
    end
  end

  def display
    cols = Array.new(column_count) do |j|
      Array.new(row_count) do |i|
        # e = element(i, j).to_f.round(4)
        e = element(i, j)
        if e.denominator == 1
          e.numerator.to_s
        else
          e.to_s
        end
      end
    end
    col_w = cols.map {|col| col.max_by(&:length).length}
    cols.transpose.map do |row|
      row.zip(col_w).map {|cell, width| cell.rjust(width)}.join(" ")
    end.map {|row| row+"\n"}.join
  end
end
